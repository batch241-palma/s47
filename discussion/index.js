// document - it refers to the whole webpage
// querySelector - it is used to select a specific object (HTML elements) fromt the document (Webpage)
const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");

// document.getElementById
// document.getElementByClass
// document.getElementByTagName
// const txtFirstName = document.getElementById("txt-first-name");
// const spanFirstName = document.getElementById("span-full-name");

// Whenever a user interacts with a webpage, this action is considered as an event
// addEventListener - function that takes two arguments
// 'keyup' - string identifying an event
// function that the listener will execute once the specified event is triggered

txtFirstName.addEventListener('keyup', (event) => {
	// innerHTML - property sets ir return the HTML content
	spanFullName.innerHTML = txtFirstName.value;
	console.log(event.target);
	console.log(event.target.value);
})
